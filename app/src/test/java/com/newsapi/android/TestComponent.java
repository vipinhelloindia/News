package com.newsapi.android;

import com.newsapi.android.db.DatabaseModule;
import com.newsapi.android.di.ActivityModule;
import com.newsapi.android.di.FragmentModule;
import com.newsapi.android.di.component.NewsApplicationComponent;
import com.newsapi.android.di.module.AppDataBaseModule;
import com.newsapi.android.di.module.AppModule;
import com.newsapi.android.di.module.NetworkModule;
import com.newsapi.android.di.module.NewsRepoModule;
import com.newsapi.android.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class,
        ActivityModule.class, FragmentModule.class,
        AppDataBaseModule.class, DatabaseModule.class,
        TestModule.class, NetworkModule.class,
        ViewModelModule.class, NewsRepoModule.class})
public interface TestComponent extends NewsApplicationComponent {
    void inject(NewsListingDaggerTest test);
}