package com.newsapi.android;

import com.newsapi.android.data.repository.NewsRepository;
import com.newsapi.android.data.repository.Resource;
import com.newsapi.android.db.NewsDBAdapter;
import com.newsapi.android.network.NetworkManager;
import com.newsapi.android.news.NewsListViewModel;
import com.newsapi.android.pojo.NewsHeadline;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TopNewsListingUnitTest {


    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    NewsListViewModel newsListViewModel;

    @Mock
    NetworkManager networkManager;

    @Mock
    NewsRepository newsRepository;

    @Mock
    NewsDBAdapter newsDBAdapter;

    @Before
    public void init() {
        newsListViewModel = new NewsListViewModel(newsRepository);
        networkManager = Mockito.mock(NetworkManager.class);
        doAnswer((Answer<Observable<Resource<NewsHeadline>>>) invocation ->
                Observable.just(Resource.success(new NewsHeadline())))
                .when(newsRepository).populateNews(any());

    }

    @Test
    public void fetchNewsServiceAssetComplete() {
//        when(networkManager.isOnline()).thenReturn(true);

        Observable<Resource<NewsHeadline>> newResourceObservable = newsRepository.populateNews(any());
        TestObserver<Resource<NewsHeadline>> userTestObserver = new TestObserver<>();
        newResourceObservable.subscribe(userTestObserver);
        userTestObserver.assertComplete();
    }
}
