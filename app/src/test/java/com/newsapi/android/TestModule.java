package com.newsapi.android;

import com.newsapi.android.api.APIInterface;
import com.newsapi.android.di.module.RetrofitModule;

import org.mockito.Mockito;

import retrofit2.Retrofit;

public class TestModule extends RetrofitModule {

    @Override
    public APIInterface provideApiInterface(Retrofit retroFit) {
        return  Mockito.mock(APIInterface.class);
    }
}