package com.newsapi.android.data.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import com.newsapi.android.api.APIInterface;
import com.newsapi.android.db.NewsDBAdapter;
import com.newsapi.android.network.NetworkManager;
import com.newsapi.android.news.NewsRequestInfo;
import com.newsapi.android.pojo.NewsHeadline;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/* domain layer with Network Bound Resource
 *@param<Q> the request type
 *@param<P> the response type
 */

@Singleton
public class NewsRepositoryManager implements NewsRepository {
    private static final String TAG = "NewsRepositoryManager";
    private NewsDBAdapter newsDBAdapter;
    private APIInterface apiInterface;
    private NetworkManager networkManager;

    @Inject
    public NewsRepositoryManager(NewsDBAdapter newsDBAdapter, APIInterface apiInterface,
                                 NetworkManager networkManager) {
        this.apiInterface = apiInterface;
        this.newsDBAdapter = newsDBAdapter;
        this.networkManager = networkManager;
    }


    @Override
    public Observable<Resource<NewsHeadline>> populateNews(final NewsRequestInfo newsRequestInfo) {
        return new NetworkBoundResource<NewsHeadline, NewsHeadline>() {

            @Override
            protected void saveCallResult(@NonNull NewsHeadline item) {
                item.setPage(newsRequestInfo.getPage());
                item.setCountry(newsRequestInfo.getCountry());
                item.setSource(newsRequestInfo.getSource());
                item.setPagesize(newsRequestInfo.getPagesize());
                newsDBAdapter.insertAll(item);
                Log.d(TAG, "saveCallResult: ");
            }

            @Override
            protected boolean shouldFetch() {
                Log.d(TAG, "shouldFetch: ");
                return networkManager.isOnline();
            }

            @NonNull
            @Override
            protected Single<NewsHeadline> loadFromDb() {
                Log.d(TAG, "loadFromDb: ");
                return newsDBAdapter.query(newsRequestInfo);
            }

            @NonNull
            @Override
            protected Observable<Resource<NewsHeadline>> createCall() {
                Log.d(TAG, "createCall: ");
                return apiInterface.getTopNews(newsRequestInfo.getCountry(), "",
                        newsRequestInfo.getPage(), newsRequestInfo.getPagesize())
                        .flatMap(newApiResponse -> Observable.just(newApiResponse == null
                                ? Resource.error("", new NewsHeadline())
                                : Resource.success(newApiResponse)));
            }
        }.getAsObservable();
    }
}
