package com.newsapi.android.data.repository;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}