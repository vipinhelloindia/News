package com.newsapi.android.data.repository;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/*Notice that each component depends only on the component one level below it.
 For example, activities and fragments depend only on a view model.*/
public abstract class NetworkBoundResource<ResultType, RequestType> {

    private static final String TAG = "NewsRepositoryManager";
    private Observable<Resource<ResultType>> result;

    @MainThread
    protected NetworkBoundResource() {
        Observable<Resource<ResultType>> source;
        if (shouldFetch()) {
            source = createCall()
                    .subscribeOn(Schedulers.io())
                    .doOnNext(apiResponse -> saveCallResult(processResponse(apiResponse)))
                    .flatMap(apiResponse -> loadFromDb().toObservable().map(Resource::success))
                    .doOnError(t -> onFetchFailed()
                    )
                    .onErrorResumeNext(t -> {
                        Log.d(TAG, "onErrorResumeNext: ");
                        return loadFromDb()
                                .toObservable()
                                .map(data -> Resource.error(t.getMessage(), data));

                    })
                    .observeOn(AndroidSchedulers.mainThread());
        } else {
            source = loadFromDb()
                    .toObservable()
                    .map(Resource::success);
        }
        result=source;
    }

    public Observable<Resource<ResultType>> getAsObservable() {
        return result;
    }

    protected void onFetchFailed() {
        Log.d(TAG, "onFetchFailed: ");
    }

    @WorkerThread
    protected RequestType processResponse(Resource<RequestType> response) {
        return response.data;
    }

    @WorkerThread
    protected abstract void saveCallResult(@NonNull RequestType item);

    @MainThread
    protected abstract boolean shouldFetch();

    @NonNull
    @MainThread
    protected abstract Single<ResultType> loadFromDb();

    @NonNull
    @MainThread
    protected abstract Observable<Resource<RequestType>> createCall();
}