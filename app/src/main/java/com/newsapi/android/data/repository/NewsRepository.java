package com.newsapi.android.data.repository;

import com.newsapi.android.news.NewsRequestInfo;
import com.newsapi.android.pojo.NewsHeadline;

import io.reactivex.Observable;

public interface NewsRepository {
    Observable<Resource<NewsHeadline>> populateNews(NewsRequestInfo newsRequestInfo);
}
