package com.newsapi.android.di;

import com.newsapi.android.news.ItemDetailActivity;
import com.newsapi.android.news.ItemListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract ItemListActivity contributeItemListActivity();

    @ContributesAndroidInjector
    abstract ItemDetailActivity contributeItemDetailActivity();

}
