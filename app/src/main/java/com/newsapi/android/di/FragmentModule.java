package com.newsapi.android.di;

import com.newsapi.android.news.ItemDetailFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract ItemDetailFragment contributeItemDetailFragment();
}
