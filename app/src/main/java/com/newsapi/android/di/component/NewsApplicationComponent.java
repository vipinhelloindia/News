package com.newsapi.android.di.component;

import android.app.Application;

import com.newsapi.android.MyApplication;
import com.newsapi.android.db.DatabaseModule;
import com.newsapi.android.di.ActivityModule;
import com.newsapi.android.di.FragmentModule;
import com.newsapi.android.di.module.AppDataBaseModule;
import com.newsapi.android.di.module.AppModule;
import com.newsapi.android.di.module.NetworkModule;
import com.newsapi.android.di.module.NewsRepoModule;
import com.newsapi.android.di.module.RetrofitModule;
import com.newsapi.android.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(modules = {AndroidInjectionModule.class, AppModule.class,
        ActivityModule.class, FragmentModule.class,
        AppDataBaseModule.class, DatabaseModule.class,
        RetrofitModule.class, NetworkModule.class,
        ViewModelModule.class,NewsRepoModule.class})
@Singleton
public interface NewsApplicationComponent {
    void inject(MyApplication myApplication);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder provideAppContext(Application application);

        NewsApplicationComponent build();
    }
}
