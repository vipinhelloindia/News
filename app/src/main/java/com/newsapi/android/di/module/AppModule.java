package com.newsapi.android.di.module;

import android.app.Application;
import android.content.Context;

import com.newsapi.android.di.ApplicationContext;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    @Provides
    @ApplicationContext
    Context provideContext(Application context){
        return context;
    }
}
