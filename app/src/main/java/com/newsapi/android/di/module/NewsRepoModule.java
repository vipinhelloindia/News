package com.newsapi.android.di.module;

import com.newsapi.android.data.repository.NewsRepository;
import com.newsapi.android.data.repository.NewsRepositoryManager;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class NewsRepoModule {
    @Binds
    abstract NewsRepository getNewsRepositoryManager(NewsRepositoryManager newsRepositoryManager);
}
