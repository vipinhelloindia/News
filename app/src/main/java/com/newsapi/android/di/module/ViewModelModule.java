package com.newsapi.android.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.newsapi.android.di.factory.ViewModelFactory;
import com.newsapi.android.di.factory.ViewModelKey;
import com.newsapi.android.news.NewsListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(NewsListViewModel.class)
    protected abstract ViewModel movieListViewModel(NewsListViewModel newsListViewModel);


}