package com.newsapi.android.di.module;

import com.newsapi.android.db.AppNewsDBHelper;
import com.newsapi.android.db.NewsDBAdapter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class AppDataBaseModule {
    @Binds
    abstract NewsDBAdapter getNewAppDataBase(AppNewsDBHelper appDBHelper);
}
