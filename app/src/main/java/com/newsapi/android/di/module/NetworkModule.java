package com.newsapi.android.di.module;

import com.newsapi.android.network.AppNetworkManager;
import com.newsapi.android.network.NetworkManager;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class NetworkModule {
    @Binds
    abstract NetworkManager getAppNetworkManager(AppNetworkManager appNetworkManager);
}
