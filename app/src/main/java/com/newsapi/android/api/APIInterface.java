package com.newsapi.android.api;

import com.newsapi.android.pojo.NewsHeadline;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {


    @GET("top-headlines?apiKey=bf1cfd602b2040939f704728dd235099")
    Observable<NewsHeadline> getTopNews(@Query("country") String country,
                                        @Query("sources") String sources,
                                        @Query("page") String page,
                                        @Query("pageSize") String pageSize);

}