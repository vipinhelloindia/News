package com.newsapi.android.news;

import android.arch.lifecycle.MutableLiveData;

import com.newsapi.android.base.BaseViewModel;
import com.newsapi.android.data.repository.NewsRepository;
import com.newsapi.android.data.repository.Resource;
import com.newsapi.android.pojo.NewsHeadline;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class NewsListViewModel extends BaseViewModel {
    private NewsRepository newsRepository;
    private MutableLiveData<Resource<NewsHeadline>> newsHeadlineMutableLiveData = new MutableLiveData<>();
    private NewsRequestInfo newsRequestInfo;
    boolean isLoading = false;

    @Inject
    public NewsListViewModel(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    public void setNewsRequestInfo(NewsRequestInfo newsRequestInfo) {
        this.newsRequestInfo = newsRequestInfo;
    }

    public void populateNews() {
        newsRepository.populateNews(newsRequestInfo)
                .doOnSubscribe(disposable -> {
                    isLoading = true;
                    addToDisposable(disposable);
                })
                .subscribe(new Observer<Resource<NewsHeadline>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Resource<NewsHeadline> newsHeadlineResource) {
                        newsHeadlineMutableLiveData.postValue(newsHeadlineResource);
                        isLoading=false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading=false;
                    }

                    @Override
                    public void onComplete() {
                        isLoading=false;
                    }
                });
    }

    public MutableLiveData<Resource<NewsHeadline>> getNewsHeadlineMutableLiveData() {
        return newsHeadlineMutableLiveData;
    }

    void loadMoreItems(int page) {
        isLoading=true;
        newsRequestInfo.setPage(page + "");
        populateNews();
    }


    boolean isLoading() {
        return isLoading;
    }
}


