package com.newsapi.android.news;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.newsapi.android.R;
import com.newsapi.android.base.BaseAppCompactActivity;
import com.newsapi.android.di.factory.ViewModelFactory;
import com.newsapi.android.pojo.Article;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ItemListActivity extends BaseAppCompactActivity {
    private static final String TAG = "ItemListActivity";

    @Inject
    ViewModelFactory viewModelFactory;

    NewsListViewModel newsListViewModel;

    private boolean mTwoPane;

    SimpleItemRecyclerViewAdapter newViewAdapter;
    int page = 1;
    int totalCount;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        newsListViewModel = ViewModelProviders.of(this, viewModelFactory).get(NewsListViewModel.class);
        newsListViewModel.getNewsHeadlineMutableLiveData().observe(this, resource -> {
            if (resource.isLoading()) {

            } else if (resource.data != null) {
                totalCount = resource.data.getTotalResults();
                Log.d(TAG, "onCreate: getNewsHeadlineMutableLiveData");
                newViewAdapter.addAll(resource.data.getArticles());
                newViewAdapter.removeLoader();
                newViewAdapter.notifyDataSetChanged();


            } else handleErrorResponse();
        });
        NewsRequestInfo newsRequestInfo = new NewsRequestInfo();
        newsRequestInfo.setCountry("us");
        newsRequestInfo.setPage(String.valueOf(page));
        newsRequestInfo.setPagesize(String.valueOf(10));
        newsListViewModel.setNewsRequestInfo(newsRequestInfo);
        newsListViewModel.populateNews();

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        RecyclerView recyclerView = findViewById(R.id.item_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        assert recyclerView != null;
        setupRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(onScrollChangeListener);


    }

    RecyclerView.OnScrollListener onScrollChangeListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
            int totalItemCount = recyclerView.getLayoutManager().getItemCount();
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

            if (!newsListViewModel.isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >=
                        totalItemCount && firstVisibleItemPosition >= 0) {
                    page++;
                    newsListViewModel.loadMoreItems(page);
                    handler.post(() -> newViewAdapter.showLoader());
                }
            }
        }

    };

    private boolean isLastPage() {
        return newViewAdapter.getItemCount() >= totalCount;
    }


    private void handleErrorResponse() {

    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        newViewAdapter = new SimpleItemRecyclerViewAdapter(this, new ArrayList<Article>(), mTwoPane);
        recyclerView.setAdapter(newViewAdapter);
    }

    @Override
    protected void onDestroy() {
        if (newsListViewModel != null) {
            newsListViewModel.onStop();
        }
        super.onDestroy();
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final ItemListActivity mParentActivity;
        private final List<Article> mValues;
        private final boolean mTwoPane;
        boolean showLoader;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Article item = (Article) view.getTag();
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(ItemDetailFragment.ARG_ITEM_ID, item.getUrl());
                    ItemDetailFragment fragment = new ItemDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_ID, item.getUrl());

                    context.startActivity(intent);
                }
            }
        };

        SimpleItemRecyclerViewAdapter(ItemListActivity parent,
                                      List<Article> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case 0:
                    return new ViewHolder(LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_list_content, parent, false));
                case 1:
                    return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.progress_bar, parent, false));
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {

            switch (getItemViewType(position)) {
                case 0: {
                    ViewHolder itemHolder = (ViewHolder) viewHolder;
                    itemHolder.mIdView.setText(mValues.get(position).getTitle());
                    itemHolder.mContentView.setText(mValues.get(position).getDescription());

                    itemHolder.itemView.setTag(mValues.get(position));
                    itemHolder.itemView.setOnClickListener(mOnClickListener);

                    Glide.with(itemHolder.mContentView.getContext())
                            .load(mValues.get(position).getUrlToImage())
                            .centerCrop()
                            .override(120, 120)
                            .into(itemHolder.mImageView);
                }

                case 1:
            }


        }

        @Override
        public int getItemViewType(int position) {
            return position < mValues.size() ? 0 : showLoader ? 1 : 0;
        }

        @Override
        public int getItemCount() {
            return mValues.size() + (showLoader ? 1 : 0);
        }

        public void showLoader() {
            showLoader = true;
            notifyItemInserted(mValues.size());
        }

        public void removeLoader() {
            showLoader = false;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mIdView;
            final TextView mContentView;
            final ImageView mImageView;

            ViewHolder(View view) {
                super(view);
                mIdView = view.findViewById(R.id.id_text);
                mContentView = view.findViewById(R.id.content);
                mImageView = view.findViewById(R.id.im_news);
            }
        }

        class ProgressViewHolder extends RecyclerView.ViewHolder {
            public ProgressViewHolder(View view) {
                super(view);
            }
        }

        void addAll(List<Article> news) {
            mValues.addAll(news);
        }
    }
}
