package com.newsapi.android.db;

import com.newsapi.android.news.NewsRequestInfo;
import com.newsapi.android.pojo.NewsHeadline;

import io.reactivex.Single;

public interface NewsDBAdapter {
    void insertAll(NewsHeadline newsHeadline);

    Single<NewsHeadline> fetchAllArticle(int count);

    Single<NewsHeadline> query(NewsRequestInfo query);

    void deleteAllHeadline();
}