package com.newsapi.android.db.convertor;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newsapi.android.pojo.Article;

import java.lang.reflect.Type;
import java.util.List;

public class ArticleListTypeConverter {

    @TypeConverter
    public List<Article> fromString(String value) {
        Type listType = new TypeToken<List<Article>>() {
        }.getType();
        List<Article> genres = new Gson().fromJson(value, listType);
        return genres;
    }

    @TypeConverter
    public String fromList(List<Article> genres) {
        return new Gson().toJson(genres);
    }
}
