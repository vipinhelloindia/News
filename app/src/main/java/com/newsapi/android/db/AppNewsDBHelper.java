package com.newsapi.android.db;

import android.arch.persistence.db.SimpleSQLiteQuery;
import android.arch.persistence.db.SupportSQLiteQuery;

import com.newsapi.android.news.NewsRequestInfo;
import com.newsapi.android.pojo.NewsHeadline;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppNewsDBHelper implements NewsDBAdapter {
    private final AppDatabase appDatabase;

    @Inject
    public AppNewsDBHelper(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public void insertAll(NewsHeadline newsHeadline) {
        appDatabase.newDao().insertAll(newsHeadline);
    }

    @Override
    public Single<NewsHeadline> fetchAllArticle(int count) {
        return appDatabase.newDao().fetchAllArticle(count);
    }


    @Override
    public Single<NewsHeadline> query(NewsRequestInfo newsRequestInfo) {
        return appDatabase.newDao().query(getSimpleQuery(newsRequestInfo));
    }

    private SupportSQLiteQuery getSimpleQuery(NewsRequestInfo newsRequestInfo) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT * FROM top_news_headline WHERE page=" + newsRequestInfo.getPage() +
                " AND country='" + newsRequestInfo.getCountry()+"'" + "ORDER BY lastUpdatedDate");
        queryBuilder.append(" LIMIT " + 1); //ORDER BY createDate DESC
        return new SimpleSQLiteQuery(queryBuilder.toString(), new String[]{});
    }

    @Override
    public void deleteAllHeadline() {
        appDatabase.newDao().deleteAllHeadline();
    }


}
