package com.newsapi.android.db;

import android.arch.persistence.db.SupportSQLiteQuery;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RawQuery;

import com.newsapi.android.pojo.NewsHeadline;

import io.reactivex.Single;

@Dao
public interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(NewsHeadline newsHeadline);

    @Query("SELECT * FROM top_news_headline LIMIT :count")
    Single<NewsHeadline> fetchAllArticle(int count);

    @RawQuery
    Single<NewsHeadline> query(SupportSQLiteQuery query);

    @Query("DELETE FROM top_news_headline")
    void deleteAllHeadline();
}
