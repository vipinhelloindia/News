package com.newsapi.android.db;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.newsapi.android.db.convertor.ArticleListTypeConverter;
import com.newsapi.android.pojo.NewsHeadline;

import static com.newsapi.android.db.AppDatabase.VERSION;

@Database(entities = {NewsHeadline.class}, version = VERSION, exportSchema = false)
@TypeConverters({ArticleListTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "new_db";
    static final int VERSION = 1;

    public abstract NewsDao newDao();
}
