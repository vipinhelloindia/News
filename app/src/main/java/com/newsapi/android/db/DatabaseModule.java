package com.newsapi.android.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.newsapi.android.di.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.newsapi.android.db.AppDatabase.DATABASE_NAME;

/*All the App data base detail will remain here*/

@Module
public class DatabaseModule {
    private static final String TAG = "DatabaseModule";
    @Provides
    @Singleton
    @TopNewDatabaseInfo
    String provideDatabaseName() {
        return DATABASE_NAME;
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(@ApplicationContext Context context, @TopNewDatabaseInfo String dbName) {
        return Room.databaseBuilder(context, AppDatabase.class, dbName).addCallback(new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
                Log.d(TAG, "onCreate: ");
            }

            @Override
            public void onOpen(@NonNull SupportSQLiteDatabase db) {
                super.onOpen(db);
                Log.d(TAG, "onOpen: ");
            }
        })
                .allowMainThreadQueries()
                .build();
    }

}
