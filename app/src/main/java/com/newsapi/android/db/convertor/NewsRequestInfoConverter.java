package com.newsapi.android.db.convertor;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.newsapi.android.news.NewsRequestInfo;

public class NewsRequestInfoConverter {

    @TypeConverter
    public NewsRequestInfo fromString(String value) {
        NewsRequestInfo newsRequestInfo = new Gson().fromJson(value,NewsRequestInfo.class);
        return newsRequestInfo;
    }

    @TypeConverter
    public String fromGenre(NewsRequestInfo genres) {
        return new Gson().toJson(String.valueOf(genres.hashCode()));
    }
}
