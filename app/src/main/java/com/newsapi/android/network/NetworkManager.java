package com.newsapi.android.network;


public interface NetworkManager {

    String connectionType();

    boolean isOnline();
}
